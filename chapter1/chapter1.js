//nama : Lalu Ibnu Hidayatullah

const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

// program tambah
const functionPenambahan = () => {

    const tambah = (obj) => {
        return parseInt(obj.nilaiA) + parseInt(obj.nilaiB);
    };

    console.log("\t==Program untuk penjumlahan==");
    rl.question('masukkan nilai a : ', function (a) {
        rl.question('masukkan nilai b : ', function (b) {
            console.log(tambah({
                nilaiA: a,
                nilaiB: b
            }));
            rl.close();
        })
    })
}

// program kurang
const functionPengurangan = () => {
    const kurang = (obj) => {
        return obj.nilaiA - obj.nilaiB;
    }

    console.log("\t==Program untuk pengurangan==");
    rl.question('masukkan nilai a : ', function (a) {
        rl.question('masukkan nilai b : ', function (b) {
            console.log(kurang({
                nilaiA: a,
                nilaiB: b
            }));
            rl.close();
        })
    })
}

// program bagi
const functionPembagian = () => {
    const bagi = (obj) => {
        return obj.nilaiA / obj.nilaiB;
    }

    console.log("\t==Program untuk pembagian==");
    rl.question('masukkan nilai a : ', function (a) {
        rl.question('masukkan nilai b : ', function (b) {
            console.log(bagi({
                nilaiA: a,
                nilaiB: b
            }));
            rl.close();
        })
    })
}

// program akar kuadrat
const functionKuadrat = () => {
    const akarKuadrat = (obj) => {
        return obj.akar ** obj.kuadrat;
    }

    console.log("\t==Program untuk Akar Kuadrat==");
    rl.question('masukkan nilai yang diakarkan : ', function (a) {
        rl.question('masukkan nilai kuadrat : ', function (k) {
            console.log(akarKuadrat({
                akar: a,
                kuadrat: k
            }));
            rl.close();
        })
    })
}

// program Luas Persegi
const functionLuasPersegi = () => {
    const luasPersegi = (obj) => {
        return obj.panjang * obj.lebar;
    }

    console.log("\t==Program untuk Luas Persegi==");
    rl.question('masukkan nilai panjang : ', function (p) {
        rl.question('masukkan nilai lebar : ', function (l) {
            console.log(luasPersegi({
                panjang: p,
                lebar: l
            }));
            rl.close();
        })
    })
}

// program volume kubus
const functionVolumeKubus = () => {
    const volumeKubus = (obj) => {
        return obj.sisi ** 3;
    }

    console.log("\t==Program untuk Volume Kubus==");
    rl.question('masukkan nilai sisi : ', function (s) {
        console.log(volumeKubus({
            sisi: s
        }));
        rl.close();
    })
}

// program volume tabung
const functionVolumeTabung = () => {
    const volumeTabung = (obj) => {
        return Math.PI * (obj.jari ** 2) * obj.tinggi;
    }

    console.log("\t==Program untuk Volume Tabung==");
    rl.question('masukkan jari-jari tabung : ', function (r) {
        rl.question('masukkan tinggi tabung : ', function (t) {
            console.log(volumeTabung({
                jari: r,
                tinggi: t
            }));
            rl.close();
        })
    })
}

// functionPenambahan();
// functionPengurangan();
// functionPembagian();
// functionKuadrat();
// functionLuasPersegi();
// functionVolumeKubus();
// functionVolumeTabung();






